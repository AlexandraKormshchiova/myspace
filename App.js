import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { ContextProvider } from './src/store/context';
import { StoriesScreen } from './src/screens/StoriesScreen';
import { CameraScreen } from './src/screens/CameraScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
        <ContextProvider>
          <NavigationComponent />
        </ContextProvider>
      </NavigationContainer>
  );
}

const NavigationComponent = () => {
  return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={StoriesScreen} />
        <Stack.Screen name="Camera" component={CameraScreen} options={{ headerShown: false }} />
      </Stack.Navigator>
  )
};
