import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';

import { StoryPreviewsCarousel } from '../components/StoryPreviewsCarousel';
import { Story } from '../components/Story';

const StoriesScreen = () => {
    StatusBar.setBarStyle('dark-content', true);

    return (
        <View style={styles.container}>
            <StoryPreviewsCarousel />
            <Story />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
    },
});

export { StoriesScreen };
