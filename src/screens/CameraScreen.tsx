import React from 'react';
import { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import Constants from 'expo-constants';

import { CameraComponent } from '../components/CameraComponent';
import { StoryPreSave } from '../components/StoryPreSave';
import { useTodoContext } from '../store/context';

const CameraScreen = () => {
    StatusBar.setBarStyle('light-content', true);

    const { stories, add } = useTodoContext();
    const navigation = useNavigation<NativeStackNavigationProp<any>>();

    const [image, setImage] = useState({ uri: '' });
    const [video, setVideo] = useState({ uri: '' });

    const goBack = () => {
        setImage({ uri: '' });
        setVideo({ uri: '' });
        navigation.goBack();
    };

    const onUploadStory = (duration: number) => {
        add({ videoUri: video.uri, imageUri: image.uri, index: stories.length, videoDuration: duration ? duration : 10000 });
        goBack();
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.closeButton} onPress={goBack}>
                <Image source={require('../../assets/close.png')} />
            </TouchableOpacity>
            {video.uri ? (
                <StoryPreSave videoUri={video.uri} onUploadStory={onUploadStory} />
            ) : (
                <CameraComponent setImage={setImage} setVideo={setVideo} />
            )}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
    closeButton: {
        padding: 20,
        position: 'absolute',
        top: Constants.statusBarHeight,
        zIndex: 100,
    }
});

export { CameraScreen };