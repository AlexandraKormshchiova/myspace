import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

type Props = {
    isRecording: boolean,
    progress: number,
    onPress: () => void,
};

const RecordButton = ({
    isRecording,
    onPress,
    progress,
}: Props) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <AnimatedCircularProgress
                rotation={0}
                size={70}
                width={3}
                fill={isRecording ? progress : 100}
                tintColor={isRecording ? '#7A3BFF' : '#fff'}
                children={() => <View style={styles.recordButton} />}
                backgroundColor="transparent" />
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    recordButton: {
        width: 60,
        height: 60,
        borderRadius: 50,
        backgroundColor: 'white',
        margin: 2,
    },
    recordButtonBorder: {
        width: 70,
        height: 70,
        borderRadius: 50,
        borderWidth: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        padding: 20,
    }
});

export { RecordButton };
