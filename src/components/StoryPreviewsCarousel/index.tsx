import React from 'react';
import { StyleSheet, ScrollView, View, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import { StoryPreview } from '../StoryPreview';
import { useTodoContext } from '../../store/context';

const StoryPreviewsCarousel = () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>();

    const { stories } = useTodoContext();

    const navigateToCameraScreen = () => {
        navigation.navigate('Camera');
    };

    return (
        <View>
            <ScrollView
                horizontal
                alwaysBounceHorizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={styles.container}
            >
                <TouchableOpacity onPress={navigateToCameraScreen} style={styles.uploadStory}>
                    <Image style={styles.uploadStoryImage} source={require('../../../assets/upload.png')} />
                </TouchableOpacity>

                {stories.map((story, index) => {
                    return <StoryPreview key={index} imageUri={story.imageUri} index={index} />;
                })}
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
    },
    uploadStory: {
        width: 100,
        height: 100,
        borderRadius: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#1e81b0',
        borderWidth: 3,
        marginRight: 4,
    },
    uploadStoryImage: {
        width: 40,
        height: 40,
    }
});

export { StoryPreviewsCarousel };