import { StyleSheet, View, TouchableOpacity, Dimensions, Image, Text } from 'react-native';
import React from 'react';
import { useEffect, useState } from 'react';
import Constants from 'expo-constants';
import { Camera, CameraType } from 'expo-camera';

import { RecordButton } from '../RecordButton';

type Props = {
    setImage: (image: any) => void,
    setVideo: (video: any) => void,
}

const CameraComponent = ({ setImage, setVideo }: Props) => {
    const [camera, setCamera] = useState(null);
    const [isRecording, setIsRecording] = useState(false);
    const [type, setType] = useState(CameraType.front);
    const [permission, requestPermission] = Camera.useCameraPermissions();

    const [seconds, setSeconds] = useState(0);

    const { height, width } = Dimensions.get('window');

    useEffect(() => {
        if (!permission?.granted) {
            requestPermission();
        }
    }, []);

    useEffect(() => {
        if (seconds > 10) {
            onStopRecording();
        }
    }, [seconds]);

    const toggleCameraType = () => {
        setType(current => (current === CameraType.back ? CameraType.front : CameraType.back));
    }

    const onStartRecording = () => {
        if (!permission?.granted) return;

        camera.recordAsync().then((video) => setVideo(video));
        setIsRecording(true);

        setSeconds(0);
        setInterval(() => setSeconds((value) => value + 1), 1000);
        camera.takePictureAsync().then((image) => setImage(image));
    };

    const onStopRecording = () => {
        camera.stopRecording();
        setIsRecording(false);
    };

    return (
        <Camera ref={ref => setCamera(ref)} style={[styles.camera, { height, width } ]} type={type}>
            <View style={styles.recordButton}>
                <RecordButton
                    progress={seconds * 10}
                    isRecording={isRecording}
                    onPress={isRecording ? onStopRecording : onStartRecording}
                />
            </View>
            <View style={styles.buttonsContainer}>
                <TouchableOpacity style={styles.button} />
                <Text style={styles.text}>История</Text>
                <TouchableOpacity style={styles.button} onPress={toggleCameraType}>
                    <Image style={styles.flipIcon} source={require('../../../assets/flip-camera.png')} />
                </TouchableOpacity>
            </View>
        </Camera>
    )
};

const styles = StyleSheet.create({
    camera: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
    },
    buttonsContainer: {
        backgroundColor: 'black',
        height: 100,
        width: '100%',
        position: 'absolute',
        right: 0,
        bottom: 0,
        paddingBottom: 20,
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    flipIcon: {
        width: 30,
        height: 30,
    },
    button: {
        padding: 20,
        width: 70,
        height: 70,
    },
    text: {
        color: 'white',
        fontSize: 20,
    },
    recordButton: {
        position: 'absolute',
        left: 0,
        width: '100%',
        bottom: 100,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeButton: {
        padding: 20,
    }
});

export { CameraComponent };