import React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity, Text, Image } from 'react-native';
import { useRef, useState } from 'react';
import { Video, ResizeMode } from 'expo-av';
import Constants from 'expo-constants';

type Props = {
    videoUri: string,
    onUploadStory: (duration: number) => void,
}

const StoryPreSave = ({ videoUri, onUploadStory }: Props) => {
    const video = useRef(null);
    const [duration, setDuration] = useState(0);

    const { height, width } = Dimensions.get('window');

    const onLoad = (data) => {
        setDuration(data.durationMillis);
    };

    const uploadStory = () => {
        onUploadStory(duration);
    };

    return (
        <View style={styles.container}>
            <Video
                ref={video}
                style={{ height, width, ...styles.video }}
                source={{
                    uri: videoUri,
                }}
                shouldPlay
                useNativeControls
                resizeMode={ResizeMode.COVER}
                onLoad={onLoad}
                isLooping
            />
            <View style={styles.buttonsContainer}>
                <TouchableOpacity style={styles.uploadButton} onPress={uploadStory}>
                    <Image style={styles.image} source={{ uri: 'https://images.unsplash.com/photo-1684939148838-3825af2260b1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=687&q=80' }} />
                    <Text style={styles.uploadButtonText}>
                        Ваша история
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    video: {
        marginTop: Constants.statusBarHeight
    },
    buttonsContainer: {
        backgroundColor: 'black',
        height: 100,
        width: '100%',
        position: 'absolute',
        right: 0,
        bottom: 0,
        paddingBottom: 20,
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    container: {
        flex: 1,
    },
    uploadButton: {
        backgroundColor: '#121212',
        borderRadius: 4,
        padding: 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    uploadButtonText: {
        color: 'white',
    },
    image: {
        marginRight: 14,
        width: 24,
        height: 24,
        borderRadius: 8,
    },
});

export { StoryPreSave };