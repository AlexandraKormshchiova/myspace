import React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';

import { useTodoContext } from '../../store/context';

type Props = {
    imageUri: string,
    index: number,
}

const StoryPreview = ({ imageUri, index }: Props) => {
    const { openModal, setActiveStoryIndex } = useTodoContext();

    const onPressPreview = () => {
        openModal();
        setActiveStoryIndex(index);
    };

    return (
        <TouchableOpacity style={styles.container} onPress={onPressPreview}>
            <Image
                style={styles.image}
                source={{ uri: imageUri}}
            />
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 6,
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
    }
});

export { StoryPreview };