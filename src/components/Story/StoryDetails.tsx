import React from 'react';
import { StyleSheet, View, Image, Text, Dimensions, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';

import { StoryThumbnails } from './StoryThumbnails';
import { StoryType, useTodoContext } from '../../store/context';

type Props = {
    activeStory: StoryType,
    activeStoryIndex: number,
    videoPositionMs: number,
}

const StoryDetails = ({ activeStory, videoPositionMs, activeStoryIndex }: Props) => {
    const { closeModal, remove } = useTodoContext();

    const onPressDelete = () => {
        remove(activeStory.index);
        closeModal();
    };

    return (
        <View style={styles.content} pointerEvents='box-none'>
            <View style={styles.statusBarBg} />
            <View style={styles.bottomBg} pointerEvents='auto'>
                <TouchableOpacity style={styles.deleteButton} onPress={onPressDelete}>
                    <Image style={styles.deleteIcon} source={require('../../../assets/delete-icon.png')} />
                </TouchableOpacity>
            </View>

            <StoryThumbnails activeStoryIndex={activeStoryIndex} activeStory={activeStory} videoPositionMs={videoPositionMs} />

            <View style={styles.userInfo}>
                <Image style={styles.image} source={{ uri: 'https://images.unsplash.com/photo-1684939148838-3825af2260b1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=687&q=80' }} />
                <View>
                    <Text style={styles.userName}>konstantin_konstantinovich</Text>
                    <Text style={styles.storyTime}>30 секунд назад</Text>
                </View>
                <TouchableOpacity style={styles.closeButton} onPress={closeModal}>
                    <Image source={require('../../../assets/close.png')} />
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    statusBarBg: {
        backgroundColor: '#000',
        height: Constants.statusBarHeight,
    },
    image: {
        marginRight: 14,
        width: 40,
        height: 40,
        borderRadius: 8,
    },
    content: {
        position: 'absolute',
        width: '100%',
        top: 0,
        bottom: 0,
        zIndex: 100,
    },
    userInfo: {
        paddingHorizontal: 10,
        display: 'flex',
        flexDirection: 'row',
    },
    userName: {
        fontWeight: '500',
        fontSize: 14,
        color: '#FFFFFF',
        marginBottom: 6,
    },
    storyTime: {
        color: '#FFFFFF',
        fontSize: 12,
    },
    bottomBg: {
        width: '100%',
        height: 100,
        top: Dimensions.get('window').height - 100,
        left: 0,
        backgroundColor: 'black',
        position: 'absolute',
        zIndex: 100000000,
        display: 'flex',
        flexDirection: 'row-reverse',
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        padding: 12,
    },
    deleteButton: {
        padding: 20,
    },
    deleteIcon: {
        width: 40,
        height: 40,
    },
});

export { StoryDetails };
