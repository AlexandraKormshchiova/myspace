import React from 'react';
import { View, Modal, Dimensions } from 'react-native';
import { useEffect, useRef, useState } from 'react';
import GestureRecognizer  from 'react-native-swipe-gestures';
import Carousel from 'react-native-reanimated-carousel';

import { StoryType, useTodoContext } from '../../store/context';
import { StoryDetails } from './StoryDetails';
import { StorySlide } from './StorySlide';

const { height, width } = Dimensions.get('window');

const Story = () => {
    const carouselRef = useRef(null);
    const { modalVisible, closeModal, activeStoryIndex, stories, setActiveStoryIndex } = useTodoContext();
    const [activeStory, setActiveStory] = useState<StoryType>({ imageUri: '', index: 0 });
    const [videoState, setVideoState] = useState<any>({});

    useEffect(() => {
        setActiveStory(stories[activeStoryIndex]);
    }, [modalVisible]);

    useEffect(() => {
        if (videoState[activeStory?.index]?.didJustFinish) {
            if (activeStoryIndex + 1 === stories.length) {
                closeModal();
                return;
            }
            setVideoState({});
            carouselRef?.current?.next();
        }
    }, [videoState]);

    useEffect(() => {
        if (modalVisible) {
            carouselRef.current?.scrollTo({ count: activeStoryIndex, animated: false });
        }
    }, [modalVisible]);

    const setVideoStatus = (status, storyIndex) => {
        setVideoState({...videoState, [storyIndex]: status});
    };

    return (
        <GestureRecognizer onSwipeDown={closeModal}>
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}>
                <View style={{ flex: 1, backgroundColor: 'black' }}>
                    <StoryDetails
                        activeStoryIndex={activeStoryIndex}
                        activeStory={activeStory}
                        videoPositionMs={videoState[activeStory?.index]?.positionMillis}
                    />
                    <Carousel
                        ref={carouselRef}
                        defaultIndex={0}
                        loop={false}
                        width={width}
                        height={height}
                        data={stories}
                        autoPlayInterval={activeStory?.videoDuration || 10000}
                        scrollAnimationDuration={200}
                        onSnapToItem={(index) => {
                            setActiveStory(stories[index]);
                            setActiveStoryIndex(index);
                        }}
                        renderItem={({ item }) => (
                            <StorySlide
                                story={item}
                                activeIndex={activeStory?.index}
                                setVideoState={setVideoStatus}
                            />
                        )}
                    />
                </View>
            </Modal>
        </GestureRecognizer>
    )
};

export { Story };
