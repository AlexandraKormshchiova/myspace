import React from 'react';
import { StyleSheet, View, Animated } from 'react-native';
import { useEffect, useState } from 'react';

import { StoryType, useTodoContext } from '../../store/context';

type Props = {
    activeStory: StoryType,
    videoPositionMs: number,
    activeStoryIndex: number,
}

const StoryThumbnails = ({ activeStory, activeStoryIndex, videoPositionMs = 0 }: Props) => {
    const { stories } = useTodoContext();

    const [percentageWidth] = useState(new Animated.Value(0));
    const lineWidth = 100 / stories.length;

    useEffect(() => {
        Animated.timing(percentageWidth, {
            toValue: videoPositionMs,
            duration: 200,
            useNativeDriver: false,
        }).start();
    }, [videoPositionMs]);

    useEffect(() => {
        percentageWidth.setValue(0);
    }, [activeStoryIndex]);

    const whiteLineWidth = percentageWidth.interpolate({
        inputRange: [0, activeStory?.videoDuration || 10000],
        outputRange: ['0%', '100%'],
    });

    return (
        <View style={styles.container}>
            {stories.map((story, index) => {
                return (
                    <View key={index} style={{ width: `${lineWidth}%`, paddingHorizontal: 2 }}>
                        {index < activeStoryIndex && (
                            <View style={[styles.whiteLine, { width: '100%' }]} />
                        )}
                        {index > activeStoryIndex && (
                            <Animated.View style={[styles.whiteLine, { width: 0 }]} />
                        )}
                        {index === activeStoryIndex && (
                            <Animated.View style={[styles.whiteLine, { width: whiteLineWidth }]} />
                        )}
                        <View style={styles.line} />
                    </View>
                );
            })}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 20,
        paddingHorizontal: 10,
        display: 'flex',
        flexDirection: 'row',
    },
    whiteLine: {
        backgroundColor: 'white',
        width: '100%',
        height: 1,
    },
    line: {
        height: 1,
        backgroundColor: '#F2F3F7',
        opacity: 0.32,
    },
});

export { StoryThumbnails };