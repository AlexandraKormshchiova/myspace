import React from 'react';
import { useEffect, useState } from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import { AVPlaybackStatus, ResizeMode, Video } from 'expo-av';

import { StoryType } from '../../store/context';

type Props = {
    story: StoryType,
    activeIndex: number,
    setVideoState: (status: AVPlaybackStatus, index: number) => void,
};

const { height, width } = Dimensions.get('window');

const StorySlide = ({ story, setVideoState, activeIndex }: Props) => {
    const [playbackObject, setPlaybackObject] = useState(null);

    useEffect(() => {
        playbackObject?.playFromPositionAsync(0);
    }, [activeIndex]);

    return (
        <View>
            {story?.videoUri ? (
                <Video
                    ref={(component) => setPlaybackObject(component)}
                    style={{ height, width }}
                    source={{ uri: story?.videoUri }}
                    shouldPlay={story.index === activeIndex}
                    resizeMode={ResizeMode.COVER}
                    progressUpdateIntervalMillis={200}
                    onPlaybackStatusUpdate={(status) => setVideoState(status, story.index)}
                />
            ) : (
                <Image style={styles.image} source={{ uri: story?.imageUri }} />
            )}
        </View>
    )
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        marginBottom: 100,
    },
    image: {
        width: '100%',
        height: '100%',
    },
});

export { StorySlide };
