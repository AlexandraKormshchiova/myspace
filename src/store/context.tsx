import React from 'react';
import { createContext, useContext, useState } from 'react';
import { StatusBar } from 'react-native';

const Context = createContext({
    stories: [],
    add: (story: StoryType) => {},
    remove: (index: number) => {},
    modalVisible: false,
    closeModal: () => {},
    openModal: () => {},
    setActiveStoryIndex: (index: number) => {},
    activeStoryIndex: null,
});

export type StoryType = {
    videoUri?: string,
    imageUri: string,
    index: number,
    videoDuration?: number,
};

function ContextProvider(props) {
    const [stories, setStories] = useState<StoryType[]>([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [activeStoryIndex, setActiveStoryIndex] = useState(false);

    const openModal = () => {
        StatusBar.setBarStyle('light-content', true);
        setModalVisible(true);
    }

    const closeModal = () => {
        StatusBar.setBarStyle('dark-content', true);
        setModalVisible(false);
    }

    function add(item: StoryType) {
        setStories([item, ...stories]);
    }

    function remove(index) {
        const newStories = stories.filter(( obj ) => {
            return obj.index !== index;
        });
        setStories(newStories);
    }

    const todoData = {
        stories,
        add,
        remove,
        modalVisible,
        closeModal,
        openModal,
        activeStoryIndex,
        setActiveStoryIndex,
    };

    return <Context.Provider value={todoData} {...props} />;
}

function useTodoContext() {
    return useContext(Context);
}

export { useTodoContext, ContextProvider };